---
title: Adopting the Open/Closed Principle in Python Development
description: This article explains the open/closed principle (OCP) and how to apply it in Python. By using this principle, you create systems that are simpler to maintain.
slug: open-close-principle
hide:
  - toc
date: 2024-01-29
authors:
  - elie
categories:
  - Clean Code Python
---

# Adopting the Open/Closed Principle in Python Development

The Open/Closed Principle (OCP), one of the five SOLID principles of object-oriented programming, plays a 
crucial role in creating scalable, maintainable, and robust software applications. 

<!-- more -->

This principle asserts that software entities (classes, modules, functions, etc.) should be open for extension 
but closed for modification. In simpler terms, this means creating systems that are easily extensible without altering the 
existing code, thus reducing the risk of bugs and facilitating future growth. 

In this article, we will explore the essence of the Open/Closed Principle and illustrate its implementation in Python, a 
language known for its simplicity and elegance.

## Understanding the Open/Closed Principle

The Open/Closed Principle, conceptualized by Bertrand Meyer, is based on the belief that once a module, 
a class, or a function is tested and deployed, it should remain unchanged. Instead of modifying existing code, 
new functionalities should be added through extensions. This approach serves several purposes:

1. **Stability**: Unchanged modules are less prone to new bugs.
2. **Reusability**: Existing code can be reused with new extensions.
3. **Maintainability**: Extending systems without altering their core facilitates maintenance.

## Implementing OCP in Python

Python, with its rich set of features, offers an excellent platform for implementing OCP. 
Python's dynamic nature, along with its support for polymorphism and inheritance, makes it well-suited for 
applying this principle.

### Case Study: Building a Reporting System

Consider a scenario where we are tasked with developing a reporting system. Initially, the system generates 
reports in a standard text format. As the system evolves, there is a need to 
support other formats like PDF and CSV.

#### Initial Approach: Without OCP

In a non-OCP approach, we might have a single `ReportGenerator` class with a `generate_report` method 
that handles report generation in all formats. This approach, while simple at first, becomes problematic 
as more formats are added, leading to a cluttered and less maintainable class.

```python
class ReportGenerator:
    def generate_report(self, data, format):
        if format == 'text':
            return self._generate_text_report(data)
        elif format == 'pdf':
            return self._generate_pdf_report(data)
        elif format == 'csv':
            return self._generate_csv_report(data)
        # More formats lead to more conditional branches
```

#### Applying OCP with Python: A Better Approach

Instead, we can redesign the system to adhere to OCP. We create an abstract base class 
`ReportGenerator` that defines a `generate_report` method. Then, we extend this base class 
for each report format.

```python
from abc import ABC, abstractmethod

class ReportGenerator(ABC):
    @abstractmethod
    def generate_report(self, data):
        pass

class TextReportGenerator(ReportGenerator):
    def generate_report(self, data):
        # Implementation for text report

class PDFReportGenerator(ReportGenerator):
    def generate_report(self, data):
        # Implementation for PDF report

class CSVReportGenerator(ReportGenerator):
    def generate_report(self, data):
        # Implementation for CSV report
```

With this design, adding a new format simply involves creating a new subclass 
of `ReportGenerator`. The existing code remains unchanged, adhering to OCP.

## Benefits of OCP in Python

The benefits of applying OCP in Python development are multiple:

1. **Increased Flexibility**: New functionalities can be added with minimal impact on existing code.
2. **Reduced Risk**: Fewer modifications to existing code mean fewer chances of introducing bugs.
3. **Simplified Testing**: Testing new extensions is simpler as the core functionality remains unchanged.

## Challenges and Considerations

While OCP offers many benefits, it also comes with its own set of challenges. One such challenge is 
the potential for over-engineering. Developers might create overly complex systems in anticipation of 
future extensions that may never be necessary. Therefore, it is crucial to balance the need for 
extensibility with simplicity in design.

## Best Practices for Implementing OCP in Python

1. **Use Abstract Classes and Interfaces**: Use Python's support for abstract base classes to define a clear contract 
for extensions.

2. **With Composition over Inheritance**: Use composition to build flexible systems without the constraints of 
rigid inheritance hierarchies.

3. **Test Extensively**: Ensure that new extensions do not break existing functionality.

4. **Document Your Code**: Maintain clear documentation to help future developers understand design choices.

## Conclusion

The Open/Closed Principle is a fundamental concept in software engineering that promotes the development of 
robust and scalable software. By adopting OCP in Python development, we can build systems that are not 
only resistant to change but also easier to maintain and extend. While challenges exist, adhering to best 
practices and being mindful of the balance between extensibility and simplicity can lead to 
successful implementation of OCP in Python projects.

