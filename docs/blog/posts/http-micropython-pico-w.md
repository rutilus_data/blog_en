---
title: How to Perform an HTTP Request in MicroPython with a Pico W?
description: This article explains how to perform an HTTP request in MicroPython with a Pico W.
hide:
  - toc
date: 2023-12-20
categories:
  - MicroPython
  - Pico W
---
# How to Perform an HTTP Request in MicroPython with a Pico W?

In this article, we will explore how to perform an HTTP request in MicroPython with a Pico W.

We will utilize the `urequests` module, which allows making HTTP requests in MicroPython.

## Prerequisites before Getting Started with Pico W

To use the Pico W, you need to have the MicroPython firmware installed on your Pico W.

You can follow the tutorial below to install the MicroPython firmware on your 
Pico W: [Install MicroPython on a Pico W](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html).

To transfer files to the Pico W, you can use the software [Thonny](https://thonny.org/) or 
PyCharm with the [MicroPython](https://plugins.jetbrains.com/plugin/9777-micropython) plugin installed.

## Connect Your Raspberry Pi Pico W to the Internet via Wi-Fi

To perform an HTTP request, you must connect your Pico W to the internet via Wi-Fi. 
We will use the `network` module from the MicroPython standard library to connect the Pico W to the internet.



```python
import network
from time import sleep

ssid = SSID
password = PASSWORD

sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)

sta_if.connect(ssid, password)
while not sta_if.isconnected():
    sleep(1)
print('Connection successful')
print(sta_if.ifconfig())
```

In this code, we start by importing the `network` module from the MicroPython standard library. Next,
we set the Wi-Fi network name and password in the `ssid` and `password` variables. Finally,
we create an instance of the `WLAN` class and connect to the Wi-Fi network.

The while loop is used to wait for the Wi-Fi network connection to be established. Once the connection is established,
we display the IP address of the Pico W.

This loop is crucial because without it, the following code will be executed before the Wi-Fi network connection is established.

To learn more about the `network` module, you can refer to the official documentation: [network — network configuration](https://docs.micropython.org/en/latest/library/network.html#module-network).

## Installing the urequests Module on Pico W


```python
# Check if the urequests module is already present
try:
    import urequests
except ImportError:
    # If the module is not present, install micropython-urequests via upip
    import upip
    upip.install('micropython-urequests')
    
    # Import urequests again after installation
    import urequests
```

In this code, we use a **try-except** structure to check if the `urequests` module is already present.
If importing generates an error (ImportError), it means the module is not installed.
In this case, we use the `upip` module to install `micropython-urequests`. Then, we import
the `urequests` module again to use it in the rest of the code.

## Making an HTTP Request in MicroPython with a Pico W

Here is an example code demonstrating how to make an HTTP request in MicroPython with a Pico W to
the [Rutilus Data](https://rutilusdata.fr/) platform. The goal is to send a value to a time series
using an HTTP POST request.

The `post` function from the `urequests` module is used to perform the HTTP request. It requires the request URL,
the data to be transmitted, and the request headers.

The data to be sent must be in JSON format, for which the `ujson` module from the MicroPython standard library
is used. It transforms a Python dictionary into JSON format. The request headers
specify that the data is in JSON format.

Don't forget to replace "token_authentication" with the token of your device, which you can find on
the corresponding page of your device on the Rutilus Data platform.


```python
import urequests
import ujson

# Replace "token_authentification" with the token of your device
data = {"value": 1.0, "token": "token_authentification"}
url = "https://rutilusdata.fr/api/timeseries/"
headers = {'content-type': 'application/json'}

# Make the HTTP POST request
response = urequests.post(url, data=ujson.dumps(data), headers=headers)

# Get the response in JSON format
json_response = response.json()

# Show the response
print(json_response)

```

To interpret the response of the request, we use the `json` function from the `urequests` module. This function 
converts the response of the request into a MicroPython dictionary.

The complete source code is available on [GitHub](https://github.com/elterrien/pico_http_example).

