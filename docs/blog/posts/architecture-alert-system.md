---
title: How to Create an Alert System for Your IoT Application?
description: This article narrates my experience in designing an alert system for my IoT platform project. Explore different approaches to implement an alert system.
date: 2024-01-22
categories:
  - PostgreSQL
  - Database
hide:
  - toc
---

# How to Create an Alert System for Your IoT Application?

This article narrates my experience in designing an alert system for my IoT platform project. 
The main goal of this system is to monitor data and trigger an alert if it exceeds a predefined threshold. 
My research aimed at finding an alert solution requiring minimal administration and easy usability.
<!-- more -->

Throughout this article, I will present three solutions I examined for my project, explaining the reasons 
that led me to choose the ultimately implemented solution.

## 1. Use an External Tool to Implement an Alert System

An external alert system is an independent tool that connects to a data source to define alert rules. Examples 
of such tools include [Signoz Alerts](https://signoz.io/docs/userguide/alerts-management/) 
and [Grafana Alerting](https://grafana.com/docs/grafana/latest/alerting/). Grafana offers an **API for configuration**, 
allowing access setup and alert creation directly from its application. In contrast, Signoz, according to 
official documentation, seems to have an API dedicated solely to log retrieval.

### Advantages of an External Tool

- **Connectivity with Multiple Data Sources**: These alert tools are generally equipped to connect to various data 
sources, facilitating the centralization of alerts within a single platform.
- **Specialized Advanced Features**: Specifically designed for alert management, these tools offer advanced features 
such as user management, multi-channel notifications, sophisticated queries combining multiple data sources, etc.
- **Quick Deployment**: These tools are ready to use, requiring only configuration to adapt to your IoT application, 
significantly reducing development time.

### Disadvantages of an External Tool

- **Tool Operation Cost**: Involves management and maintenance, whether for an open-source solution requiring continuous 
administration or a proprietary solution requiring a financial subscription.
- **Technical Skill Requirement**: Requires acquiring knowledge of a new tool and its specific query language.
- **Dependency Link**: The tool becomes an essential component of your IoT application, limiting flexibility to 
adapt it to your specific needs.

## 2. Control Incoming Data Directly in the IoT Application

An alternative is to proactively manage incoming data within the application. When an API request is received, a 
function performs a check to determine if an alert is configured and if the data in question can trigger an alert.

### Advantages of Proactive Data Management

- **Integration**: This approach integrates directly with the IoT application, providing complete control of the system.
- **Flexibility**: It is possible to create a custom alert system independent of external tools, offering increased 
flexibility.

### Disadvantages of Proactive Data Management

- **Additional Development**: Implementing this approach requires creating a custom alert system, which can take a 
significant investment of time.
- **Multiple Protocols**: While this approach works effectively with a single data exchange protocol, its complexity 
increases when multiple protocols are used.

## 3. Exploit the Native Features of Your Database

The final approach is to exploit the advanced features of your database.

For example, **InfluxDB** offers a built-in **alert feature** that triggers alerts based on the value of a field. 
The generated alerts can be sent to a **Slack** channel or trigger an **HTTP request**.

**MongoDB** provides the ability to create **triggers that can be activated by CRUD events or scheduled at a given 
frequency. These triggers can be used to generate alerts. It is important to note that this feature is available only 
for MongoDB Atlas-managed clusters.

Similarly, **triggers** are available in **PostgreSQL**. They can be triggered by CRUD events or invoke functions. 
Since PostgreSQL is an open-source database, its use incurs no additional cost.

### Advantages of Exploiting the Native Features of Your Database

- **Performance**: This approach reduces latency by avoiding data transfer to an external tool.
- **Ease**: Alerts are triggered directly at the data source, and the system can handle multiple exchange protocols 
without additional complexity.

### Disadvantages of Exploiting the Native Features of Your Database

- **Query Language**: Using the native features of the database requires mastering its specific query language.
- **Dependency**: This approach makes the application dependent on the database, potentially limiting its flexibility.
- **Additional Development**: Implementing this approach requires a tool capable of checking if a trigger has been 
activated and sending an alert accordingly.

## Conclusion: Which Approach Did I Choose?

After carefully examining these three approaches, I chose to exploit the native features of my database. In my case, 
I opted for TimescaleDB, an extension of PostgreSQL.

**Why this approach?**

The first reason is the use of multiple data exchange protocols. While initially considering controlling incoming 
data directly in my application, that would have introduced additional complexity to the system.

The second reason is my preference for a solution requiring minimal administration. Being alone on this project, 
this criterion held particular importance.

Lastly, adopting this approach provided an opportunity to acquire skills in using PostgreSQL triggers, an advanced 
feature. This skill can be applied to other use cases, such as updating a field when data is modified or historical 
data tracking.

In an upcoming article, I will detail the practical implementation of this approach. In the meantime, feel free 
to contact me on my [Twitter profile](https://twitter.com/El_Terrien) or through my [GitHub account](https://github.com/elterrien) if you have any questions or comments.
