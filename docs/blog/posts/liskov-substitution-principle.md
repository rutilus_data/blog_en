---
title: Understanding the Liskov Substitution Principle in Python
description:  This article delves into the essence of LSP, offering insights into its practical application in Python programming.
slug: liskov-substitution-principle
hide:
  - toc
date: 2024-01-31
authors:
  - elie
categories:
  - Clean Code Python
---

# Understanding the Liskov Substitution Principle in Python: A Comprehensive Guide

In the realm of object-oriented programming, the Liskov Substitution Principle (LSP) stands as a fundamental pillar, 
playing a crucial role in ensuring robust and maintainable software designs. Named after computer scientist Barbara 
Liskov, this principle is integral to the SOLID principles, a set of guidelines aimed at promoting scalable, 
understandable, and flexible coding practices. This article delves into the essence of LSP, offering insights into 
its practical application in Python programming.

## What is the Liskov Substitution Principle?

LSP asserts that objects of a superclass should be replaceable with objects of its subclasses without affecting the 
correctness of the program. In simpler terms, if class B is a subclass of class A, then wherever class A is used, 
class B should be able to serve as its substitute without introducing errors or unexpected behavior.

The principle is not just about method signatures and return types but also about ensuring that the subclass maintains 
the behavior expected by the superclass. It's about adhering to the contract established by the superclass.

## Why is Liskov Substitution Principle Important?

The significance of LSP lies in its ability to ensure that a software system remains easy to understand and modify. 
By adhering to LSP, developers can:

1. **Enhance Code Reusability**: Ensuring subclasses remain true to their parent classes' behavior makes them more 
versatile and reusable.
2. **Improve Code Maintainability**: LSP-compliant code is generally cleaner and more organized, making it easier 
to maintain and extend.
3. **Reduce Bugs and Errors**: By maintaining consistent behavior across a class hierarchy, LSP helps in avoiding 
bugs that can arise from unexpected behavior of subclasses.

## LSP in Python: A Practical Guide

### Example of Python Code Violating LSP

Here's a Python example that illustrates a violation of the Liskov Substitution Principle (LSP):

```python
class Bird:
    def fly(self):
        return "I can fly!"

class Penguin(Bird):
    def fly(self):
        raise NotImplementedError("Cannot fly!")

def let_bird_fly(bird: Bird):
    print(bird.fly())

# Usage
blue_bird = Bird()
let_bird_fly(blue_bird)  # Works fine

happy_feet = Penguin()
let_bird_fly(happy_feet)  # Raises NotImplementedError
```

In this example, the `Bird` class has a method `fly()`. The `Penguin` class, a subclass of `Bird`, overrides 
the `fly()` method but changes its behavior drastically by raising a `NotImplementedError`, indicating that penguins 
cannot fly. This violates LSP because `Penguin` objects cannot be used as substitutes for `Bird` objects without 
altering the correctness of the program, specifically in the `let_bird_fly` function. This function expects 
any `Bird` object (or its subclass) to fly, but `Penguin` breaks this expectation by changing the behavior 
of the `fly()` method.

### Refactoring the Code to Adhere to LSP

To refactor the provided Python code to adhere to the Liskov Substitution Principle (LSP), we need to restructure 
the class hierarchy so that subclasses of `Bird` only extend it if they can fulfill its contract 
(i.e., if they can implement all its methods without changing their expected behavior). Here's how the 
refactored code would look:

```python
class Bird:
    def move(self):
        return "I can move!"

class FlyingBird(Bird):
    def fly(self):
        return "I can fly!"

class Penguin(Bird):
    # Penguin doesn't override fly method
    pass

def let_bird_fly(bird: FlyingBird):
    print(bird.fly())

# Usage
eagle = FlyingBird()
let_bird_fly(eagle)  # Works fine

happy_feet = Penguin()
# let_bird_fly(happy_feet)  # This line will now result in a type error, not a behavior error
```

In this refactored version, `Bird` is a general class without the `fly` method. We create a new 
subclass `FlyingBird` that includes birds capable of flying and thus implements the `fly` method. 
This structure ensures that only flying birds are passed to functions or scenarios where flying is expected, 
adhering to LSP by ensuring that subclasses can be used interchangeably with their base class without altering 
the program's correctness.

### LSP in Action: Practical Python Example

To demonstrate the Liskov Substitution Principle (LSP) in action, consider a payment processing system in Python. 
This example will show how subclasses can be used interchangeably with their base class without changing the program's 
behavior, adhering to LSP.

```python
class Payment:
    def process_payment(self, amount):
        raise NotImplementedError

class CreditCardPayment(Payment):
    def process_payment(self, amount):
        return f"Processing credit card payment for {amount}"

class DebitCardPayment(Payment):
    def process_payment(self, amount):
        return f"Processing debit card payment for {amount}"

class PayPalPayment(Payment):
    def process_payment(self, amount):
        return f"Processing PayPal payment for {amount}"

def process_transaction(payment_method: Payment, amount):
    print(payment_method.process_payment(amount))

# Usage
credit_payment = CreditCardPayment()
process_transaction(credit_payment, 100)

debit_payment = DebitCardPayment()
process_transaction(debit_payment, 100)

paypal_payment = PayPalPayment()
process_transaction(paypal_payment, 100)
```

In this example, each subclass (`CreditCardPayment`, `DebitCardPayment`, `PayPalPayment`) of the `Payment` class 
implements the `process_payment` method. The `process_transaction` function can operate with any of these payment 
methods, demonstrating that subclasses can be substituted for the superclass (`Payment`) without affecting the 
function's behavior, thus adhering to LSP.

## Conclusion of LSP in Python

The Liskov Substitution Principle is more than a guideline; it's a foundation for creating effective and reliable 
object-oriented software. By understanding and applying LSP in Python programming, developers can build systems 
that are not only efficient but also scalable and easy to maintain. It’s a principle that underscores the importance 
of thoughtful class design, ensuring that subclasses truly represent an is-a relationship with their superclasses.

Remember, the power of LSP lies in its simplicity and its profound impact on the integrity and robustness of software design.


