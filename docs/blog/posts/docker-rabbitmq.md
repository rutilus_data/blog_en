---
title: Deploying RabbitMQ with Docker Compose
description: This tutorial explains how to deploy RabbitMQ with Docker Compose and we will cover how to configure RabbitMQ with a custom configuration file.
hide:
  - toc
date: 2023-12-20
categories:
  - Message Queuing
  - RabbitMQ
---
# Tutorial: Deploying RabbitMQ with Docker Compose

## Step 1: Install Docker and Docker Compose

Make sure Docker and Docker Compose are installed on your machine. You can download and install them from the official Docker site: [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/).

## Step 2: Create a Docker Compose File

Create a file named `docker-compose.yml` and paste the provided Docker Compose configuration.

```yaml
version: '3'

services:

  rabbitmq:
    image: rabbitmq:3.12-management-alpine
    container_name: 'rabbitmq_api_client'
    volumes:
      - ./rabbitmq/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf
    environment:
      - RABBITMQ_DEFAULT_USER=user
      - RABBITMQ_DEFAULT_PASS=password
      - RABBITMQ_CONFIG_FILE=/etc/rabbitmq/rabbitmq.conf
    ports:
      - 5672:5672
      - 15672:15672

```

### Understanding the Docker Compose Configuration for RabbitMQ

The provided Docker Compose configuration creates a Docker container named rabbitmq_api_client using the Docker 
image rabbitmq:3.12-management-alpine. The container is configured to use port 5672 for AMQP connections and port 
15672 for the RabbitMQ management interface. The container is also configured to use a custom RabbitMQ configuration file.

Some details about the configuration:

- **Volume**: Mounts a volume, linking the local ./rabbitmq/rabbitmq.conf file to the /etc/rabbitmq/rabbitmq.conf file 
in the container. This allows copying the custom RabbitMQ configuration file into the container.
- **RABBITMQ_DEFAULT_USER**: Sets the default username for RabbitMQ to connect to the RabbitMQ management interface.
- **RABBITMQ_DEFAULT_PASS**: Sets the default password for RabbitMQ to connect to the RabbitMQ management interface.
- **RABBITMQ_CONFIG_FILE**: Sets the path of the custom RabbitMQ configuration file within the container.


## Step 3: Create a RabbitMQ Configuration File

Create a directory named rabbitmq in the same location as your docker-compose.yml file. Inside the rabbitmq directory, 
create a file named rabbitmq.conf. This file can be used to customize RabbitMQ settings.

## Step 4: Configure RabbitMQ with a Custom Configuration File (Optional)
Open the rabbitmq.conf file on your machine and add custom configurations if needed. For example:

```ini
# rabbitmq.conf
vm_memory_high_watermark.absolute = 2GB
```

This sets RabbitMQ's high memory watermark limit to 2 GB. If you wish to add more configuration parameters, you 
can find the complete list of configuration parameters in the official RabbitMQ documentation: [Configuration](https://www.rabbitmq.com/configure.html#configuration-files).

## Step 5: Deploy RabbitMQ with Docker Compose

Open a terminal, navigate to the directory containing your docker-compose.yml file, and execute the following command:

```bash
docker-compose up
```

This command will download the RabbitMQ Docker image, create a Docker container named rabbitmq_api_client based on the 
provided configuration, and start the RabbitMQ server in the background.

Upon launch, you should see in the logs whether your configuration file has been recognized:

```bash
rabbitmq      |   Config file(s): /etc/rabbitmq/rabbitmq.conf
rabbitmq      |                   /etc/rabbitmq/conf.d/10-defaults.conf
rabbitmq      | 
rabbitmq      |   Starting broker...2023-11-29 07:36:10.216117+00:00 [info] <0.230.0> 
rabbitmq      |  node           : rabbit@b0b0b0b0b0b0
rabbitmq      |  home dir       : /var/lib/rabbitmq
rabbitmq      |  config file(s) : /etc/rabbitmq/rabbitmq.conf
```

## Step 6: Access the RabbitMQ Management Interface

Open your web browser and go to http://localhost:15672/. Log in with the username `user` and password `password` as 
specified in the Docker Compose file.

## Step 7: Connect to RabbitMQ via AMQP

You can now connect to RabbitMQ using the AMQP protocol on `localhost:5672` with the credentials provided in the Docker 
Compose file.

Congratulations! You have successfully deployed RabbitMQ using Docker Compose. You can further customize the 
configuration by modifying the docker-compose.yml and rabbitmq.conf files according to your specific needs.