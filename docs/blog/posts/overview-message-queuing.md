---
title: Comparison of Message Queuing Solutions for IoT
description: Message queuing solutions are tools that facilitate communication between applications. 
  They are used in various domains, particularly in IoT, to enable communication between sensors and 
  data processing applications.
hide:
  - toc
date: 2023-12-20
categories:
  - Message Queuing
---
# Comparative Analysis of Message Queuing Solutions for IoT

## Introduction

Message queuing solutions are tools that facilitate communication between applications. They are used in various domains, 
particularly in IoT, to enable communication between sensors and data processing applications.

These solutions enable asynchronous communication between applications, meaning the sending application does not wait 
for a response from the receiving application. This asynchronous communication helps decouple applications, preventing 
the sending application from being blocked.

Messages are sent to queues and processed by receiving applications in the order they were sent.

Message queuing solutions are powerful tools but can be challenging to grasp.

This document aims to present and compare the most commonly used message queuing solutions in IoT.



## Basic Concepts of Message Queuing Solutions

The basic concept of MQTT solutions is as follows: the producer sends a message to the broker, which stores it in a 
queue. Subsequently, the consumer retrieves the message from the queue and processes it.

``` mermaid
sequenceDiagram
    participant Producer
    participant Broker
    participant Consumer
    Producer->>Broker: Message
    Consumer->>Broker: Message
```

### Les messages

Messages are data sent by applications to the message broker. They can be binary or structured in formats like JSON, XML, etc.

### Les queues

Queues are waiting lines where messages are sent. Typically, queues operate on a First In First Out (FIFO) basis, 
meaning messages are processed in the order they were sent.

### Le producer

The producer is the application responsible for sending messages to the broker. Multiple producers can exist for a single queue.

### Le consumer

The consumer is the application that processes messages in the queue.

## Solutions de message queuing

### RabbitMQ 

[RabbitMQ](https://www.rabbitmq.com/) is an open-source message broker written in Erlang. It ensures messages reach their destination 
and stores them in queues.

### Mosquitto

[Mosquitto](https://mosquitto.org/) is an open-source message broker written in C. Widely used in IoT, it is lightweight and consumes minimal resources.

### Redis

[Redis](https://redis.io/) is an open-source database management system written in C. Data is stored directly in RAM, providing quick 
responses. Redis is not only a database but also a message broker.

Redis includes a message queuing module in Python [RedisMQ](https://pypi.org/project/redismq/) and other languages.

