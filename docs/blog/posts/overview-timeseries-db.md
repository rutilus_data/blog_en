---
title: Comparative Analysis of Time Series Databases for IoT
description: Time series databases are optimized for the storage and retrieval of temporal data. They uniquely store data with a timestamp, allowing queries based on this timestamp.
hide:
  - toc
date: 2023-12-20
categories:
  - Database
  - Time Series
---
# Comparative Analysis of Time Series Databases for IoT

Time series databases are optimized for the storage and retrieval of temporal data. They uniquely store data with a 
timestamp, allowing queries based on this timestamp.

Use cases are diverse, including sensors, monitoring, weather, IoT, networks, and web applications.

## Characteristics of Time Series Databases

Time series databases share common features that set them apart from other database types:

- **Timestamping**: All data entries are timestamped and indexed with a timestamp.
- **Compression**: Time series databases are designed to compress data. To learn more about the compression algorithms 
used, refer to [this article](https://www.timescale.com/blog/time-series-compression-algorithms-explained/).
- **Partitioning**: Data entries are partitioned based on time or another database field.
- **Retention**: Data is automatically deleted after a specified time period, limiting database size and reducing storage costs.

## InfluxDB - The All-in-One Time Series Database

[InfluxDB](https://www.influxdata.com/), written in Go, developed by InfluxData, offers an open-source version 
(clustering functionality not included) and a Cloud offering.

The integrated graphical interface of InfluxDB facilitates user management, data visualization, alert creation, and 
plugin configuration, enhancing the user experience for exploring and understanding temporal data.

InfluxDB provides data collection plugins for monitoring (docker, amazon, hardware, AMQP, GitHub, etc.), simplifying 
data recording in the database. Clients in various languages are available, allowing querying of InfluxDB's REST API.

A distinctive feature is the use of the "FLUX" query language, specifically designed for manipulating temporal data, 
providing dedicated syntax for time series queries.

### Data Structure in InfluxDB

Data in InfluxDB is structured as follows:

- **Bucket**: A bucket is a data container, comparable to a database in a relational database.
- **Measurement**: A measurement (temperature, consumption, etc.) is a dimension of a time series, comparable to a 
table in a relational database.
- **Tag**: A tag is a label that filters data, comparable to a column.
- **Field**: A field is a numeric value, comparable to a column.
- **Timestamp**: A timestamp is a temporal value, comparable to a column.

For more information on data structuring in InfluxDB, refer to the [InfluxDB documentation](https://docs.influxdata.com/influxdb/v2/get-started/#data-organization).

## MongoDB - The Versatile NoSQL Database

MongoDB, a NoSQL database written in C++, developed by MongoDB Inc., offers an open-source version and a Cloud offering.

MongoDB is a document-oriented and versatile database. By default, it is not configured to store temporal data, 
requiring some operations to enable the storage and querying of temporal data.

### Data Structure in MongoDB

Data in MongoDB is structured as follows:

- **Database**: A database is a data container, comparable to a database in a relational database.
- **Collection**: A collection is a set of documents, comparable to a table in a relational database.
- **Document**: A document is a JSON-formatted record, comparable to a row in a relational database. In a document, 
temporal data, tags, fields, and lists of values can be stored.

For more information on data structuring in MongoDB, refer to the [MongoDB documentation](https://docs.mongodb.com/manual/core/data-modeling-introduction/).

## TimeScaleDB - PostgreSQL-Based Database for Time Series

[TimeScaleDB](https://www.timescale.com/) is an open-source time series database built on PostgreSQL, developed by 
Timescale Inc., which also offers a Cloud offering.

TimeScaleDB is an extension of PostgreSQL, enabling the storage and querying of temporal data using SQL. It is compatible 
with PostgreSQL clients.

### Data Structure in TimeScaleDB

Data in TimeScaleDB is structured similarly to PostgreSQL, with tables referred to as hypertables.

The hypertable is a table partitioned based on time, allowing the creation of dimensions with tags. Storing 
the same data with the same timestamp is not allowed, preventing duplicates.

## Choosing the Right Time Series Database

Each database management system has unique advantages and disadvantages. Here are some guidelines to help you choose 
the most suitable database for your use case.

- **TimeScaleDB**: Leverages PostgreSQL's maturity, enabling the use of PostgreSQL clients to connect to the database 
and benefit from SQL. Frameworks compatible with PostgreSQL are also supported by TimeScaleDB. For example, Django, 
with its ORM, provides support for TimeScaleDB.

- **InfluxDB**: Specialized in time series, easy to learn and use. Data is structured to record temporal information 
with mandatory "measurement" and "timestamp" fields. However, storage is exclusively dedicated to time series, and for 
other needs (project management, users, etc.), another database may be required. InfluxDB offers over 300 plugins to 
facilitate data collection, but using the "FLUX" query language is required to query the database.

- **MongoDB**: A versatile NoSQL database that is easy to use. Specific database configuration is required for storing 
temporal data. MongoDB is suitable for applications not requiring massive data storage (less than 1000 points per second). 
If adding metadata to time series is necessary, MongoDB may be a suitable solution. However, querying JSON-formatted 
data may be more complex compared to a relational database.



