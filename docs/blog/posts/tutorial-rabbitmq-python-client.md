---
title: Tutorial on RabbitMQClient Python Client for RabbitMQ
description: RabbitMQClient is a Python library that simplifies interaction with RabbitMQ servers by providing a convenient API. It abstracts the complexities of direct communication with RabbitMQ's HTTP API, allowing developers to manage vhosts, queues, exchanges, and more using Pythonic syntax.
hide:
    - toc
date: 2023-12-20
categories:
    - Message Queuing
    - RabbitMQ
    - Python
---

# Tutorial on RabbitMQClient: Python Client for RabbitMQ

## Introduction

RabbitMQ is a powerful message broker that facilitates communication between distributed systems. Its robust and flexible 
architecture makes it a popular choice for implementing reliable messaging patterns. In this tutorial, we will explore 
the capabilities of RabbitMQClient, a Python library designed to interact with RabbitMQ servers through its API.

## Why RabbitMQ?

Before diving into RabbitMQClient, let's briefly understand why RabbitMQ is a crucial component in modern software 
architectures. RabbitMQ enables asynchronous communication between different components of a system, providing a 
scalable and decoupled approach to message handling. This results in improved system reliability, performance, and 
maintainability.

## What is RabbitMQClient?

[RabbitMQClient](https://github.com/elterrien/rabbitmq_api_client) is a Python library that simplifies interaction with RabbitMQ servers by providing a convenient 
API. It abstracts the complexities of direct communication with RabbitMQ's HTTP API, allowing developers to manage 
vhosts, queues, exchanges, and more using Pythonic syntax.

Throughout this tutorial, we will cover the installation and configuration of RabbitMQClient, explore its various 
features, and learn how to perform common operations on RabbitMQ servers. Whether you are a beginner or an experienced 
developer, this tutorial will guide you through the fundamentals of RabbitMQClient, enabling you to incorporate it 
into your messaging infrastructure.

Now, let's proceed to the installation and configuration of RabbitMQClient.

## Installation

To get started with RabbitMQClient, follow these steps to install the library and set up your Python environment.

### 1. Install RabbitMQClient

You can install RabbitMQClient using the Python package manager, pip. Open your terminal or command prompt and run 
the following command:


```bash
pip install rabbitmq-api-client
```

### 2. Verify the Installation

After installation, you can verify that RabbitMQClient is installed correctly by running the following command:

```bash
pip show rabbitmq-api-client
```

This command should display information about the installed package, confirming a successful installation.

### 3. Import RabbitMQClient into Your Python Code

You can now start using RabbitMQClient in your Python scripts or projects. Import it at the beginning of your Python code:

```python
from rabbitmq_api_client import RabbitMQClient
```

### 4. Provide RabbitMQ Server Details

Create an instance of the RabbitMQClient class by providing the base URL of your RabbitMQ server 
and authentication details (username and password):

```python
from rabbitmq_api_client import RabbitMQClient

rabbitmq_client = RabbitMQClient(base_url="http://votre-serveur-rabbitmq", 
                                 username="votre-nom-utilisateur", 
                                 password="votre-mot-de-passe")
```

With these steps, you have successfully installed RabbitMQClient and are ready to explore its features. 
In the following sections, we will delve into the configuration of RabbitMQClient and its usage to interact 
with RabbitMQ servers.

## Example of Using RabbitMQClient to Create a Queue


```python
from rabbitmq_api_client import RabbitMQClient
from rabbitmq_api_client.schemas import CreateQueue

rabbitmq_client = RabbitMQClient(base_url="http://votre-serveur-rabbitmq", 
                                 username="votre-nom-utilisateur", 
                                 password="votre-mot-de-passe")

queue_schema = CreateQueue(name="queue_name", auto_delete=False, durable=True) 
rabbitmq_client.create_queue(vhost="votre-vhost", 
                             queue=queue_schema)
```

In this example, the queue will not be deleted when the last consumer disconnects because the `auto_delete` parameter 
is set to `False`. Also, the queue and messages will not be deleted when the RabbitMQ server restarts because 
the `durable` parameter is set to `True`.

