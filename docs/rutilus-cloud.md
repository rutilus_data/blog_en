---
hide:
  - navigation
  - toc
---
# Rutilus Data Cloud: IoT Solution to Simplify IoT Application Development

Rutilus Cloud is an IoT solution designed to simplify the development of IoT applications. It allows managing
sensors, storing data, and visualizing it without worrying about the infrastructure.

The features of Rutilus Cloud include:

* **Sensor Management**: handling sensors, registration, authentication, etc.
* **Data Storage**: recording sensor data in a time series database.
* **Data Visualization**: displaying sensor data in graphs with easy data filtering capabilities.
* **Communication Protocols**: the application has a REST API for managing sensors, recording data,
and retrieving data. It also features an AMQP message broker to receive sensor data and store it in the database.

Sign up for free on Rutilus Data: [https://rutilusdata.com/](https://rutilusdata.com/)

